#ifndef DIALER_PI_INCOMINGCALL_H
#define DIALER_PI_INCOMINGCALL_H

#include <QtCore/QUrl>
#include <QtQml/QQmlApplicationEngine>
#include <QApplication>
#include <QString>
#include <QObject>
#include <QLabel>
#include <QGridLayout>


class IncomingCall : public QWidget{
    Q_OBJECT
public:
    IncomingCall(QString callPath, QString phoneNumber, QWidget *parent = 0);
    ~IncomingCall();
    void showIncoming();

public slots:
    void answer();
    void hang();

private:
    QString callPath;
    QString phoneNumber;
    QSize screenSize;
    QGridLayout *commonLayout;
    QWidget *incomingWindow;
    void createCommonLayout();

};
#endif // DIALER_PI_INCOMINGCALL_H
