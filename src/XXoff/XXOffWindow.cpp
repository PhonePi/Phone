#include <QtWidgets/QTextEdit>
#include <QtWidgets/QPushButton>
#include <QScreen>
#include <QDebug>
#include <iostream>
#include <QtDBus/QDBusInterface>
#include "XXOffWindow.h"
#include <sys/socket.h>
#include <sys/un.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

#define I2C_CMD_BATTERY     0xBB    // "Check battery status" (frequency == 60 sec.)
#define I2C_CMD_CHECK       0xCC    // "Check general status" (frequency == 1 sec.)
#define I2C_CMD_BRIGTH      0xDD    // "Set display brightness" (async.)
#define I2C_CMD_SHUT        0x88    // "RPi shut down" (async.)
                                    // OTHER commands are invalid

XXOffWindow::XXOffWindow(QWidget *parent)
        : QWidget(parent)
{
    screenSize.setWidth(300);
	screenSize.setHeight(100);
    createCommonLayout();
}

XXOffWindow::~XXOffWindow(){

}

void XXOffWindow::setPath(int argc, char *argv[]){
    if (argc > 1) socket_path=argv[1];
}

void XXOffWindow::showWindow(){
    mainWindow = new QWidget();
    mainWindow ->setAutoFillBackground(true);
    QPalette pal(this->palette());
    pal.setColor(QPalette::Background, "#fbf1c7");
    mainWindow ->setPalette(pal);
    mainWindow->setFixedSize(screenSize.width(), screenSize.height());
    mainWindow->setMinimumSize(screenSize.width(), screenSize.height());
	mainWindow->setWindowTitle("Power off?");
    mainWindow->activateWindow();
    mainWindow->setLayout(commonLayout);
    mainWindow->show();
}

void XXOffWindow::shutTheFuckUpKhmOff(){
    struct sockaddr_un addr;
    char buf[100];
    int fd;

    if ( (fd = socket(AF_UNIX, SOCK_STREAM, 0)) == -1) {
        perror("socket error");
        exit(-1);
    }

    memset(&addr, 0, sizeof(addr));
    addr.sun_family = AF_UNIX;
    if (*socket_path == '\0') {
        *addr.sun_path = '\0';
        strncpy(addr.sun_path+1, socket_path+1, sizeof(addr.sun_path)-2);
    } else {
        strncpy(addr.sun_path, socket_path, sizeof(addr.sun_path)-1);
    }

    if (::connect(fd, (struct sockaddr*)&addr, sizeof(addr)) == -1) {
        perror("connect error");
        exit(-1);
    }

    buf[0] = I2C_CMD_SHUT;
    if (write(fd, buf, 1) != 1) {
        fprintf(stderr,"write error");
        exit(-1);
    }

    if (read(fd,buf,1) != 1) {
        fprintf(stderr,"read error");
        exit(-2);
    }
	
    fprintf(stderr, "Shutdown: %s(%d)\n", (buf[0] == 0) ? "OK" : "FAIL", buf[0]);
}

void XXOffWindow::close(){
    exit(0);
}

void XXOffWindow::createCommonLayout(){
    QGridLayout* textLayout = new QGridLayout();
	
	QLabel *question = new QLabel("Do you want to power off?");
	QFont textFont = question->font();
	textFont.setPointSize(15);
	textFont.setBold(false);
	question->setFont(textFont);

	QPushButton *yes = new QPushButton("Yes", this);
	yes->setFixedSize(55, 30);
	yes->setFont(textFont);
	yes->setFlat(true);
    connect(yes, SIGNAL(clicked()), this, SLOT(shutTheFuckUpKhmOff()));

	QPushButton *no = new QPushButton("No", this);
	no->setFixedSize(55, 30);
	no->setFont(textFont);
	no->setFlat(true);
    connect(no, SIGNAL(clicked()), this, SLOT(close()));
	
	textLayout->addWidget(yes, 0, 0, Qt::AlignCenter);
	textLayout->addWidget(no, 0, 1, Qt::AlignCenter);

    commonLayout = new QGridLayout();
    commonLayout->setSpacing(10);
	commonLayout->addWidget(question, 0, 0, Qt::AlignCenter | Qt::AlignTop);
    commonLayout->addLayout(textLayout, 1, 0, Qt::AlignCenter);
}
