#ifndef XXOFF_WINDOW_H
#define XXOFF_WINDOW_H 

#include <QtCore/QUrl>
#include <QtQml/QQmlApplicationEngine>
#include <QApplication>
#include <QString>
#include <QObject>
#include <QLabel>
#include <QGridLayout>


class XXOffWindow : public QWidget{
    Q_OBJECT
public:
    XXOffWindow(QWidget *parent = 0);
    ~XXOffWindow();
    void showWindow();
	void setPath(int, char **);

public slots:
    void close();
	void shutTheFuckUpKhmOff();

private:
    QGridLayout *commonLayout;
    QWidget *mainWindow;
    QSize screenSize;
	char *socket_path = "\0hidden";
    void createCommonLayout();
};
#endif // XXOFF_WINDOW_H
