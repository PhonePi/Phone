//
// Created by kivi on 31.05.17.
//

#ifndef SETTINGS_APP_BRIGHTNESS_H
#define SETTINGS_APP_BRIGHTNESS_H

void readBrightness(int newVal);
void brightness();
void setBrightness(char val);

int getBattery();
double getTemperature();

#endif //SETTINGS_APP_BRIGHTNESS_H
