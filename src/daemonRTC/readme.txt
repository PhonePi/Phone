# Install
1. Open the file `/boot/config.txt` for editing;
2. Add next string at end of the file:
`dtparam=i2c_arm=on`
3. Open the file `/etc/modules-load.d/raspberrypi.conf` for editing and add next strings:
`snd-bcm2835`
`i2c-bcm2835`
`i2c-dev`
`rtc-ds1307`
4. Make reboot of system;
5. Execute command `sudo i2cdetect -y 1` after booting for test of success.